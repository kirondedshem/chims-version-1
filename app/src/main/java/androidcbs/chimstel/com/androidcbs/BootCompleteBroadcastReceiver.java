package androidcbs.chimstel.com.androidcbs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Receives the broadcast that a device has finished booting and starts the background service for transmitting sms messages
 * received by the device
 */
public class BootCompleteBroadcastReceiver extends BroadcastReceiver {

    public BootCompleteBroadcastReceiver() {
    }

    @Override
    public void onReceive( final Context context, Intent intent) {
        Log.d("Boot Complete action",intent.getAction());
        //if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
        if (Intent.ACTION_BOOT_COMPLETED.equalsIgnoreCase(intent.getAction()) || ("android.intent.action.QUICKBOOT_POWERON"== intent.getAction())) {


            try {
                Intent smsTransmitterIntent = new Intent(context, PeriodicSMSTransmitterService.class);
                context.startService(smsTransmitterIntent);

            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }

            try {

                Intent timerService = new Intent(context, TimerService.class);
                context.startService(timerService);

            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }

            try {

                Intent attendanceLogService = new Intent(context, PeriodicTimeLoggingService.class);
                context.startService(attendanceLogService);
            }
            catch (Exception exc)
            {
                exc.printStackTrace();
                Log.e("Start Service Failure",exc.getMessage());
            }


        }


    }
}
