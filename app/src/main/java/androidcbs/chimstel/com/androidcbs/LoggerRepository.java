package androidcbs.chimstel.com.androidcbs;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class LoggerRepository {
    private DatabaseHelper databaseHelper;
    public LoggerRepository(DatabaseHelper databaseHelper){
        this.databaseHelper = databaseHelper;
    }

    public void insert(LogObject logObject)
    {
        try{
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(LogObject.LOGGER_MESSAGE_DEVICE_IMEI, logObject.getIMEI());
            values.put(LogObject.LOGGER_MESSAGE_SIM_SERIAL, logObject.getSerial());
            values.put(LogObject.LOGGER_MESSAGE_ID,  logObject.getLoggerMsgId());
            values.put(LogObject.LOGGER_MESSAGE_LATITUDE,logObject.getLatitude());
            values.put(LogObject.LOGGER_MESSAGE_LONGITUDE,logObject.getLongitude());
            values.put(LogObject.LOGGER_MESSAGE_CONTENT,logObject.getMessageContent());
            values.put(LogObject.LOGGER_MESSAGE_NETWORK_TIME, logObject.getNetworkTime());
            db.insert(LogObject.LOGGER_TABLE, null, values);
            db.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void delete(String logMsgId) throws Exception {
        try{
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            String sql = "DELETE FROM " + LogObject.LOGGER_TABLE + " WHERE " +LogObject.LOGGER_MESSAGE_ID  + "= ?";
            db.execSQL(sql, new String[]{logMsgId});
            db.close();
            Log.i("LOG Repository", "LOG Deleted");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public List<LogObject> getAllLoggerMessages(){
        List<LogObject> logMessageList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + LogObject.LOGGER_TABLE;
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                LogObject logObject = new LogObject();

                logObject.setLoggerMsgId(cursor.getString(0));
                logObject.setNetworkTime(cursor.getString(1));
                logObject.setSerial(cursor.getString(2));
                logObject.setLatitude(cursor.getString(3));
                logObject.setLongitude(cursor.getString(4));
                logObject.setMessageContent(cursor.getString(5));
                logObject.setIMEI(cursor.getString(6));
                logMessageList.add(logObject);

            }while(cursor.moveToNext());
            cursor.close();
        }
        db.close();
        return logMessageList;
    }
}
