package androidcbs.chimstel.com.androidcbs;

public class LogObject {
    public static String LOGGER_TABLE="logger";
    //object properties
    private String networkTime;
    private String latitude;
    private String longitude;
    private String IMEI;
    private String serial;
    private String loggerMsgId;
    private String messageContent;

    //columns
    public static String LOGGER_MESSAGE_ID = "message_id";
    public static String LOGGER_MESSAGE_LATITUDE = "latitude";
    public static String LOGGER_MESSAGE_LONGITUDE = "longitude";
    public static String LOGGER_MESSAGE_CONTENT = "content";
    public static String LOGGER_MESSAGE_DEVICE_IMEI = "device_imei";
    public static String LOGGER_MESSAGE_SIM_SERIAL = "sim_serial";
    public static String LOGGER_MESSAGE_NETWORK_TIME = "networktime";

    public String getNetworkTime()
    {
        return networkTime;
    }

    public void setNetworkTime(String networkTime){
        this.networkTime = networkTime;
    }

    public String getIMEI()
    {
        return  IMEI;
    }
    public void setIMEI(String IMEI)
    {
        this.IMEI =IMEI;
    }

    public String getSerial()
    {
        return  serial;
    }
    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    public String getLatitude()
    {
        return this.latitude;
    }
    public void setLatitude(String latitude)
    {
        this.latitude=latitude;
    }

    public String getLongitude()
    {
        return this.longitude;
    }
    public void setLongitude(String longitude)
    {
        this.longitude=longitude;
    }

    public String getMessageContent()
    {
        return this.messageContent;
    }
    public void setMessageContent(String messageContent)
    {
        this.messageContent =messageContent;
    }

    public String getLoggerMsgId()
    {
        return this.loggerMsgId;
    }
    public void setLoggerMsgId(String loggerMsgId)
    {
        this.loggerMsgId=loggerMsgId;
    }

    @Override
    public String toString() {
        return this.getIMEI() + " " + this.getNetworkTime();
    }
}
