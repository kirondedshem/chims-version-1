package androidcbs.chimstel.com.androidcbs;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.List;

public class PeriodicTimeLoggingService extends Service {
    private List<LogObject> logObjects;
    private LoggerRepository loggerRepository;
    public PeriodicTimeLoggingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Toast.makeText(getBaseContext() ,"created service",Toast.LENGTH_LONG);
        if(loggerRepository == null) loggerRepository = new LoggerRepository(new DatabaseHelper(getBaseContext()));
        logObjects = loggerRepository.getAllLoggerMessages();
        //Toast.makeText(getBaseContext() ,"successfully gor messages",Toast.LENGTH_LONG);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        final android.os.Handler handler = new android.os.Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.i("Period SMS Transmitter", "Sms transmitter onHandle Intent - runnable section");
                SyncData();
                handler.postDelayed(this, 5 * 60 * 1000);
            }
        }, 60 * 1000);
        return START_STICKY;
    }

    /**
     * Synchronizes the data with the backend in the case that we have any un-logged transactions' data
     */
    private void SyncData(){
        try{
            // check if we have anything to transmit
            if(logObjects.isEmpty()) return;

            // read an sms from the list and transmit it
            for(LogObject logSms :logObjects){
                Log.i("Unsent Log Object SMS", logSms.toString());
                Integer result = new LogTransmitterService().sendLogObject(logSms);
                switch(result){
                    case 200:case 409:
                        loggerRepository.delete(logSms.getLoggerMsgId());
                        break;
                    default: break;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            logObjects = loggerRepository.getAllLoggerMessages();
        }
    }
}
