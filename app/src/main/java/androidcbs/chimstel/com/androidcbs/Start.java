package androidcbs.chimstel.com.androidcbs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class Start extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);


        try
        {
            // Periodic SMS Transmitter Service
            Intent smsTransmitterIntent = new Intent(this, PeriodicSMSTransmitterService.class);
            this.startService(smsTransmitterIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            // Location Service
            Intent locationIntent = new Intent(this, NetworkLocationService.class);
            this.startService(locationIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            // Periodic Logs Transmitter
            Intent logServiceIntent = new Intent(this, PeriodicTimeLoggingService.class);
            this.startService(logServiceIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            // Time and Attendance Logs Service
            Intent timerIntent = new Intent(this, TimerService.class);
            this.startService(timerIntent);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Start Service Failure",exc.getMessage());
        }


        try
        {
            Button btn_refresh = (Button)this.findViewById(R.id.btn_refresh);
            btn_refresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    UpdateInterface();

                    Toast.makeText(Start.this,"Content refreshed",Toast.LENGTH_SHORT).show();
                }
            });
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Button Event",exc.getMessage());
        }





        UpdateInterface();



       // Toast.makeText(getApplicationContext(), "Services Started", Toast.LENGTH_SHORT).show();
    }




    private void UpdateInterface()
    {







        try
        {
            TextView imei = (TextView)this.findViewById(R.id.imei);
            TextView sim_serial = (TextView)this.findViewById(R.id.sim_serial);
            TelephonyManager telephonyManager = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
            imei.setText(telephonyManager.getDeviceId());
            sim_serial.setText(telephonyManager.getSimSerialNumber());
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Info",exc.getMessage());
        }



        try
        {
            TextView last_recorded_on = (TextView)this.findViewById(R.id.last_recorded_on);
            SharedPreferences sharedPref = this.getBaseContext().getSharedPreferences(this.getBaseContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String defaultValue = "Never";
            String value = sharedPref.getString(this.getBaseContext().getString(R.string.last_sms_recieved_time),defaultValue);
            last_recorded_on.setText(value);

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Session",exc.getMessage());
        }



        try
        {
            TextView last_communicated_on = (TextView)this.findViewById(R.id.last_communicated_on);;
            SharedPreferences sharedPref = this.getBaseContext().getSharedPreferences(this.getBaseContext().getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            String defaultValue = "Never";
            String value = sharedPref.getString(this.getBaseContext().getString(R.string.last_server_communication_time),defaultValue);
            last_communicated_on.setText(value);

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Session",exc.getMessage());
        }


        try
        {
            TextView transaction_count = (TextView)this.findViewById(R.id.transaction_count);
            List<SMSMessage> smsMessageList;
            SMSRepository smsRepository = null;
            if(smsRepository == null) smsRepository = new SMSRepository(new DatabaseHelper(getBaseContext()));
            smsMessageList = smsRepository.getAllSMSMessages();
            if(smsMessageList.isEmpty())
            {
                String none = "0";
                transaction_count.setText(none);
            }
            else
            {
                Integer the_size = smsMessageList.size();
                transaction_count.setText(the_size.toString());
            }

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            Log.e("Cant read Sms cont",exc.getMessage());
        }



        try
        {
            TextView internet_availability = (TextView)this.findViewById(R.id.internet_availability);
            boolean isAvailable = internetIsAvailable();
            if(isAvailable)
            {
                String value  = "Yes";
                internet_availability.setText(value);
            }
            else
            {
                String value  = "No";
                internet_availability.setText(value);
            }

        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }


    }



    private boolean internetIsAvailable(){
        try{
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();

        }catch(Exception exc){
            exc.printStackTrace();
            return false;
        }
    }



}
